!#/bin/bash
echo -e "\033[0;32mUpdate dan upgrade...\033[0m"
pkcon refresh -y && sudo pkcon update -y;
echo -e "\033[0;32mRemove Vlc...\033[0m"
pkcon remove vlc -y
echo -e "\033[0;32mInstall basic, codec and font...\033[0m"
pkcon install git kio-gdrive bluez-obexd gwenview ffmpegthumbs kffmpegthumbnailer usb-creator-kde kamera ttf-mscorefonts-installer fonts-font-awesome ttf-bitstream-vera testdisk kubuntu-restricted-extras kcron kubuntu-driver-manager icc-profiles icc-profiles-free -y
echo -e "\033[0;32mInstall KDE Apps...\033[0m"
pkcon install kate kcalc kdeconnect partitionmanager krdc kmail kget kdf kcolorchooser kfind k3b dragonplayer amarok sweeper vokoscreen ktorrent akregator konversation kleopatra -y
echo -e "\033[0;32mInstall Productive Apps...\033[0m"
pkcon install inkscape gimp optipng fontforge libreoffice libreoffice-kde libreoffice-style-breeze ktouch -y
echo -e "\033[0;32mInstall Telegram...\033[0m"
sudo snap install telegram-desktop hugo-y
echo -e "\033[0;32mLast, Autoremove...\033[0m"
sudo apt autoremove -y
echo -e "\033[0;32mUpdate and Installing. DONE...\033[0m"
exit
